import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * @author Nattapat Sukpootanan.
 * ConverterUI this class for creating UI.
 * */
public class ConverterUI extends JFrame{

	private JButton convertButton;
	private JButton clearButton;
	private UnitConverter uc;
	private JTextField unitText1;
	private JTextField unitText2;
	private JComboBox unitComboBox1;
	private JComboBox unitComboBox2;
	private JLabel label;
	/**
	 * Constructor for creating UI.
	 * */
	public ConverterUI( UnitConverter uc ) {
		super("Length Converter");
		this.uc = uc;
		initComponents( );
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

	}
	/**
	 * Initialize all of Components.
	 * */
	private void initComponents() {
		Container contents = this.getContentPane();
		LayoutManager layout = new FlowLayout( );
		contents.setLayout( layout );
		convertButton = new JButton("Convert");
		clearButton = new JButton("Clear");
		unitText1 = new JTextField(10);
		unitText2 = new JTextField(10);
		unitComboBox1 = new JComboBox(uc.getUnits());
		unitComboBox2 = new JComboBox(uc.getUnits());
		label = new JLabel("=");

		unitText2.setEditable(false);

		contents.add(unitText1);
		contents.add(unitComboBox1);
		contents.add(label);
		contents.add(unitText2);
		contents.add(unitComboBox2);
		contents.add( convertButton );
		contents.add(clearButton);
		ActionListener convertListener = new ConvertButtonListener( );
		convertButton.addActionListener( convertListener );
		ActionListener clearListener = new ClearButtonListener( );
		clearButton.addActionListener(clearListener);
		unitText1.addActionListener(convertListener);

	}
	/**
	 * run and show visible.
	 */
	public void run(){
		this.pack();
		this.setVisible(true);
	}
	/**
	 * ConvertButtonLisnter for get listener of convert button.
	 * and convert.
	 * */
	class ConvertButtonListener implements ActionListener{

		@Override
		/**
		 * convert amount in  unit to  another unit  and show in next textField
		 * if input is not a number it will warning textFeild will be red.
		 * */
		public void actionPerformed(ActionEvent e) {
			String text1 = unitText1.getText().trim();
			if(text1.equals("")){
				unitText1.setBackground(Color.white);
				unitText2.setText("");
			}
			if ( text1.length() > 0 ) {
				try{
					double value = Double.valueOf( text1 );
					unitText1.setBackground(Color.white);
					double result = 0;
					result =    uc.convert(value,(Unit)unitComboBox1.getSelectedItem(),(Unit)unitComboBox2.getSelectedItem());
					unitText2.setText(result+"");
				}
				catch(NumberFormatException nfe){
					unitText1.setBackground(Color.red);

				}
			}

		}

	}
	/**
	 * ClearButtonListener for get listener of clear button.
	 * */
	class ClearButtonListener implements ActionListener{

		@Override
		/**
		 *clear all text in both TextField.
		 * */
		public void actionPerformed(ActionEvent e) {
			unitText1.setText("");
			unitText2.setText("");

		}

	}

}
