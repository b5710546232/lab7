/**
 * @author Nattapat Sukpootanan.
 * Unit interface that have method convertTo,getValue and toString.
 * */
public interface Unit {
	/**
	 * convertTo that convert in that unit that wanted.
	 * @param unit that want to  convert to
	 * @param amount that want to convert to in that unit.
	 * @return result that converted in double.
	 * */
	public double convertTo(Unit unit, double amount);
	/**
	 *getValue of base unit.
	 *@return value of base unit. 
	 * */
	public double getValue();
	/**
	 *toString get string format.
	 *@return unit format in string. 
	 * */
	public String toString();
}
