/**
 * @author Nattpat Sukpootanan
 * UnitConverterApp is application class for running.
 * */
public class UnitConverterApp {
	/**main for running 
	 * @param agrs is not used.
	 * */
	public static void main(String[] agrs){
		UnitConverter uc =  new UnitConverter();
		ConverterUI ui = new ConverterUI (uc);
		ui.run();
		
	}
}
