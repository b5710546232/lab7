/**
 * @author Nattapat Sukpootanan.
 * Length enum of Length that have set of unit and base value.
 * */
public enum Length implements Unit {
	METER("Meter", 1.0),CENTIMETER("Centimeter",0.01), KILOMETER("Kilometer", 1000.0), MILE("Mile", 1609.344), FOOT(
			"Foot", 0.3048), WA("Wa", 2), MICRON("Micron", 1.0E-6), YARD(
			"Yard", 3 * 0.348);
	public final String name;
	public final double value;
/**
 * Constructor  create Length.
 * @param name of this unit in length.
 * @param value of this unit in length. 
 * */
	private Length(String name, double value) {
		this.name = name;
		this.value = value;
	}
	/**
	 * getValue get value of each base value in that unit
	 * @return value of that base unit. 
	 * */
	public double getValue() {
		return value;
	}
	/**
	 * getString get name of unit.
	 * @return string name format of each unit.
	 * */
	public String getString() {
		return name;
	}
	/**
	 * convertTo  convert amount to that unit.
	 * @return  result of that converted in double.
	 * */
	@Override
	public double convertTo(Unit unit, double amount) {
		return this.getValue()*amount/unit.getValue();
	}
	/**
	 * toString get string format.
	 * @return  string format of this unit name.
	 * */
	@Override
	public String toString() {
		return this.name;
	}
}
