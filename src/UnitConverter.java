/**
 * @author Nattapat Sukpootanan
 * The UnitConverter class receives requests from the UI to converts a value from one unit to another.
 * */
public class UnitConverter {
	/**
	 * convert - convert Unit to another unit that selected.
	 * @param amount that want to converted.
	 * @param fromUnit stater Unit.
	 * @param toUnit output Unit.
	 * @return result in double.
	 * */
	public double convert(double amount,Unit fromUnit,Unit toUnit){
		return amount*fromUnit.getValue()/toUnit.getValue();
	}
	
	/**
	 * getUnits - get all Unit that have in Length in array.
	 * @return  array of Unit in enum Length.
	 * */
	public Unit [] getUnits(){
		return Length.values();
	}
	
}
